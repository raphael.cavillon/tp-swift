import Kitura
import HeliumLogger

HeliumLogger.use()

let router = Router()

//le middleware permet de reconnaitre les types de fichier
router.all(middleware: BodyParser())

//on déclare un array qui contient les infos
var student =   [["Id" : "0", "Nom": "Aubarbier", "Prénom": "Benjamin"],
    ["Id" : "1", "Nom": "Cavillon", "Prénom": "Raphael"],
    ["Id" : "2", "Nom": "Same", "Prénom": "Tyranus"]
    ]

router.get("/") { request, response, next in
    response.send("<html><head><meta charset='utf-8'/></head><body><div><form action='/' method='POST'><input type='text' name='name' placeholder='tape un truc'><input type='submit' value='Rechercher'></form></div></body></html>")
    next()
}

//function pour rechercher dans le tableau https://stackoverflow.com/questions/25678373/split-a-string-into-an-array-in-swift
func filterName(searchName: String) -> String {
    var names : String = ""
    for i in 0...student.count-1{
        if student[i]["Nom"]!.lowercased().contains(searchName.lowercased()) || student[i]["Prénom"]!.lowercased().contains(searchName.lowercased()){
            names += student[i]["Nom"]!+" "+student[i]["Prénom"]!+"-"
        }
    }
    return names
}

//on déclare un array qui contient les infos https://stackoverflow.com/questions/25678373/split-a-string-into-an-array-in-swift
router.post("/") { request, response, next in
    if let trie = request.body?.asURLEncoded?["name"] {
        let search = filterName(searchName: trie)
        if search != "" {
            var splitstudent = search.components(separatedBy: "-")
            splitstudent.removeLast()
            for i in 0...splitstudent.count-1{
                let eleve: [String] = splitstudent[i].components(separatedBy: " ")
                for trieid in 0...student.count-1{
                    if student[trieid]["Nom"] == eleve[0]{
                        let idstudent = student[trieid]["Id"]
                        response.send("<html><head><meta charset='utf-8'/></head><body><li><a href='/infostudent/\(idstudent!)'=>\(splitstudent[i])</a></li></html></body>")
                    }
                }
            }
        }else{
            response.status(.notFound)
        }
    } else {
        response.status(.notFound)
    }
    next()
}

router.get("/infostudent/:xxx") { request, response, next in
    
    response.send("<html><head><meta charset='utf-8'/> </head><div>Moyenne élève = 10</div><br><div>Mediane élève = 10</div><br><div>Ecart type = 10</div>")

    next()
}


Kitura.addHTTPServer(onPort: 8080, with: router)
Kitura.run()
